sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Folder", {

		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/diagram")
				.done(function(data){
					var oData = new sap.ui.model.json.JSONModel(data);
					console.log(oData);
					ctrl.getView().setModel(oData, "diagram");
				});
		},
		onPress: function(oEvent){
			var oObject = oEvent.getSource().getBindingContext("diagram");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("categorylist", {
				identify: oItem.FolderGuid,
				/*query: {
					test: oItem.AutoCounter
				}*/
			});
		}

	});

});