sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"nnext/iq/Processing/model/Formatter"
], function(Controller,History,Formatter) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.FolderList", {
		oFormatter: Formatter,
		onInit: function() {
			var ctrl = this;
			
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			//	$.ajax("/Flow7Api/api/diagram")
			$.ajax("/Flow7Api/api/dashboard/processing/folder")
				.done(function(data){
					var oData = new sap.ui.model.json.JSONModel(data);
					console.log(oData);
					ctrl.getView().setModel(oData, "diagram");
				});
		},
		onPress: function(oEvent){
			var oObject = oEvent.getSource().getBindingContext("diagram");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("list2", {
				identify: oItem.Key.Identify
				/*query: {
					test: oItem.AutoCounter
				}*/
			});
		},
		onPressMasterBack: function(oEvent) {
		
			this._oRouter.navTo("initialized" );
			
		},
	});

});