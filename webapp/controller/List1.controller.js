sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.List1", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nnext.iq.Processing.view.List1
		 */
		//	onInit: function() {
		//
		//	},
		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/dashboard/processing/folder")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					ctrl.getView().setModel(oData, "processing");
				});
		}
	});

});