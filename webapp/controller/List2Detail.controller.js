sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/Fragment",
	"sap/ui/model/json/JSONModel",
	"jquery.sap.global",
	"sap/ui/core/routing/History",
], function(Controller,jQuery, Fragment, JSONModel,History) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.List2Detail", {

		onInit: function(oEvent) {
			this.getOwnerComponent().getRouter().getRoute("list2detailshow").attachPatternMatched(this._onRouteMatched, this);
		},
		_onRouteMatched: function(oEvent) {
			var sRequisitionId = oEvent.getParameter("arguments").RequisitionId;
			var aFilter = [];
			var ctrl = this;
			//oBinding.Filter(aFilter);
			$.ajax("/Flow7Api/api/fdp/m/"+sRequisitionId)
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "requisitions");
					console.log(oData);
				//	aFilter.push(new Filter("Identify", FilterOperator.Contains, sRequisitionId));
				  //this.getView().bindElement("/RequisitionId/"+sRequisitionId);
				//	var oList = ctrl.getView().byId("tableList2");
				//	var oBinding = oList.getBinding("items");
				//	oBinding.filter(aFilter);
					
					ctrl._showFormFragment("Display");
					
					// to navigate to the page on phone and not show the split screen items
					//var oSplitContainer = ctrl.byId("FormSplitscreen");
				//	oSplitContainer.toDetail(this.createId("DetailShow"));

				});
			
		},
		
			_formFragments: {},


		_getFormFragment: function (sFragmentName) {
			var oFormFragment = this._formFragments[sFragmentName];

			if (oFormFragment) {
				return oFormFragment;
			}

			oFormFragment = sap.ui.xmlfragment(this.getView().getId(), "nnext.iq.Processing.view." + sFragmentName);

			this._formFragments[sFragmentName] = oFormFragment;
			return this._formFragments[sFragmentName];
		},
		
		_showFormFragment : function (sFragmentName) {
			var oPage = this.getView().byId("DetailShow");

			oPage.removeAllContent();
			oPage.insertContent(this._getFormFragment(sFragmentName));
		},
		onPressMasterBack: function(oEvent) {
			window.history.go(-1);
		}
		
	});

});