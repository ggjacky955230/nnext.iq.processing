sap.ui.define([], function() {
	"use strict";

	return {
		processStep: function(fValue) {
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[1];
		},
		
		processApprover: function(fValue){
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[1];
		},
		processingCount: function(nCount) {
			if (nCount > 10) {
				return "Error";
			} else if (nCount > 5) {
				return "Warning";
			} else {
				return "Success";
			}
		},
	};
});